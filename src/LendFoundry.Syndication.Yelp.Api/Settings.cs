﻿using System;


namespace LendFoundry.Syndication.Yelp
{
    /// <summary>
    /// Settings
    /// </summary>
    public class Settings {
        /// <summary>
        /// service name
        /// </summary>
        /// <returns></returns>
        public static string ServiceName => Environment.GetEnvironmentVariable ($"CONFIGURATION_NAME") ?? "yelp";
    }
}