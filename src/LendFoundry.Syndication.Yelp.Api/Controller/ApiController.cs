﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Yelp;
using LendFoundry.Syndication.Yelp.Request;
using LendFoundry.Syndication.Yelp.Response;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace LendFoundry.Syndication.Yelp.Api.Controller {
    /// <summary>
    /// ApiController
    /// </summary>
    [Route ("/")]
    public class ApiController : ExtendedController {
        #region Public Constructors

        /// <summary>
        /// ApiController costructor
        /// </summary>
        /// <param name="service">The service.</param>
        public ApiController (IYelpService service) 
        {
            Service = service;
        }

        #endregion Public Constructors

        #region Private Properties

        /// <summary>
        /// Gets the service.
        /// </summary>
        /// <value>The service.</value>
        private IYelpService Service { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// Gets the business details.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="businessId">The business identifier.</param>
        /// <param name="countryCode">The country code.</param>
        /// <param name="language">The language.</param>
        /// <param name="langFilter"></param>
        /// <param name="actionlinks"></param>
        /// <returns>ActionResult</returns>
        [HttpGet ("{entitytype}/{entityid}/{businessId}/businesses")]
        [ProducesResponseType (typeof (IBusinessDetailsResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> GetBusinessDetails (string entityType, string entityId, string businessId, string countryCode, string language, bool langFilter, bool actionlinks) 
        {
            return await ExecuteAsync (async () => {
                return Ok (await Task.Run (() => Service.GetBusinessDetails (entityType, entityId, businessId, countryCode, language, langFilter, actionlinks)));
            });
        }

        /// <summary>
        /// Gets the phone details.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="phoneNumber">The phone number.</param>
        /// <returns>ActionResult</returns>
        [HttpGet ("{entitytype}/{entityid}/businesses/search/{phoneNumber}/phone")]
        [ProducesResponseType (typeof (ISearchResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> GetPhoneDetails (string entityType, string entityId, string phoneNumber) 
        {
            return await ExecuteAsync (async () => {
                return Ok (await Task.Run (() => Service.GetPhoneDetails (entityType, entityId, phoneNumber)));
            });
        }

        /// <summary>
        /// Serches the details.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="term">The term.</param>
        /// <param name="location">The location.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="sort">The sort.</param>
        /// <param name="categoryFilter">The category filter.</param>
        /// <param name="radiusFilter">The radius filter.</param>
        /// <param name="dealsFilter">if its true then set deals filter.</param>
        /// <returns>Task ActionResult</returns>
        [HttpGet ("{entitytype}/{entityid}/businesses/search/{term}/{location}")]
        [ProducesResponseType (typeof (ISearchResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> SearchDetails (string entityType, string entityId, string term, string location, int limit, int offset, int sort, string categoryFilter, int radiusFilter, bool dealsFilter) 
        {
            return await ExecuteAsync (async () => {
                return Ok (await Task.Run (() => Service.SearchDetails (entityType, entityId, term, location, limit, offset, sort, categoryFilter, radiusFilter, dealsFilter)));
            });
        }
        
        /// <summary>
        /// Business Match details
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost ("{entitytype}/{entityid}/businesses/match")]
        [ProducesResponseType (typeof (ISearchResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> GetBusinessMatchDetails (string entityType, string entityId, [FromBody] BusinessMatchSearchRequest request) {
            return await ExecuteAsync (async () => {
                return Ok (await Task.Run (() => Service.GetBusinessMatchDetails (entityType, entityId, request)));
            });
        }

        #endregion Public Methods
    }
}