﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Syndication.Yelp
{
    public interface IYelpConfiguration :  IDependencyConfiguration
    {
        #region Public Properties

        string ApiBaseUrl { get; set; }
        string BusinessApiUrl { get; set; }
        string ConsumerKey { get; set; }
        string ConsumerSecret { get; set; }
        string GrantType { get; set; }
        string PhoneApiUrl { get; set; }
        string BusinessMatchApiUrl {get; set;}
        string SearchApiUrl { get; set; }
        string Token { get; set; }
        string TokenSecret { get; set; }
        string PhonePrefix { get; set; }
        string ConnectionString { get; set; }

        #endregion Public Properties
    }
}