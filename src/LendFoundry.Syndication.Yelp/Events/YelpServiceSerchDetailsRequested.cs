﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.Yelp.Events
{
    public class YelpServiceSearchDetailsRequested : SyndicationCalledEvent
    {
    }
}