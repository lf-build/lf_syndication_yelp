﻿namespace LendFoundry.Syndication.Yelp.Request
{
    public class SearchRequest : ISearchRequest
    {
        #region Public Properties

        public string Attributes { get; set; }
        public string Categories { get; set; }
        public decimal Latitude { get; set; }
        public int Limit { get; set; }
        public string Locale { get; set; }
        public string Location { get; set; }
        public decimal Longitude { get; set; }
        public int OffSet { get; set; }
        public int OpenAt { get; set; }
        public bool OpenNow { get; set; }
        public string Price { get; set; }
        public int Radius { get; set; }
        public string SortBy { get; set; }
        public string Term { get; set; }

        #endregion Public Properties
    }
}