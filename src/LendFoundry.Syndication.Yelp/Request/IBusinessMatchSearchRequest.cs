namespace LendFoundry.Syndication.Yelp.Request
{
    public interface IBusinessMatchSearchRequest
    {
        string Name {get; set;}

        string Address1 {get;set;}

        string Address2 {get; set;}

        string Address3 {get;set;}

        string City {get;set;}

        string State {get;set;}

        string Country {get;set;}

        double Latitude {get;set;}

        double Longitude {get; set;}

        string Phone {get; set;}

        string Zip_Code {get;set;}

        string Yelp_Business_id {get; set;}

        int Limit {get; set;}

        string Match_Threshhold {get; set;}
    }
}