namespace LendFoundry.Syndication.Yelp.Request
{
    public class BusinessMatchSearchRequest : IBusinessMatchSearchRequest
    {
        public BusinessMatchSearchRequest()
        {

        }
        public string Name {get; set;}
        public string Address1 {get;set;}
        public string Address2 {get; set;}
        public string Address3 {get;set;}
        public string City {get;set;}
        public string State {get;set;}
        public string Country {get;set;}
        public double Latitude{get;set;}
        public double Longitude {get; set;}
        public string Phone {get; set;}
        public string Zip_Code {get;set;}
        public string Yelp_Business_id {get; set;}
        public int Limit {get; set;}
        public string Match_Threshhold {get; set;}
    }
}