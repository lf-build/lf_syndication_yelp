﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.Yelp
{
    [Serializable]
    internal class YelpException : Exception
    {
        public string ErrorCode { get; set; }
        public YelpException() { }
        public YelpException(string message) : this(null, message, null)
        {

        }
        public YelpException(string errorCode, string message) : this(errorCode, message, null)
        {

        }
        public YelpException(string message, Exception innerException) : this(null, message, innerException)
        {

        }
        public YelpException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }
        protected YelpException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}