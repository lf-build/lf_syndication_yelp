﻿using LendFoundry.Syndication.Yelp.Response.Models;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Yelp.Response
{
    public class SearchResponse : ISearchResponse
    {
        #region Public Constructors

        public SearchResponse()
        {
        }

        public SearchResponse(Proxy.Response.ISearchResponse phoneSearchResponse)
        {
            if (phoneSearchResponse != null)
            {
                Total = phoneSearchResponse.Total;
                if (phoneSearchResponse.Businesses != null)
                    CopyToBusinessList(phoneSearchResponse.Businesses);
            }
        }

        #endregion Public Constructors

        #region Public Properties

        [JsonConverter(typeof(InterfaceListConverter<IBusinessDetailsResponse, BusinessDetailResponse>))]
        public IList<IBusinessDetailsResponse> Businesses { get; set; }

        public int Total { get; set; }

        #endregion Public Properties

        #region Private Methods

        private void CopyToBusinessList(IList<Proxy.Response.IBusiness> businesList)
        {
            Businesses = new List<IBusinessDetailsResponse>();
            foreach (var item in businesList)
            {
                Businesses.Add(new BusinessDetailResponse(item));
            }
        }

        #endregion Private Methods
    }
}