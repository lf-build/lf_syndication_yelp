﻿namespace LendFoundry.Syndication.Yelp.Response.Models
{
    public interface ICoordinate
    {
        #region Public Properties

        double Latitude { get; set; }
        double Longitude { get; set; }

        #endregion Public Properties
    }
}