﻿namespace LendFoundry.Syndication.Yelp.Response.Models
{
    public class User : IUser
    {
        #region Public Constructors

        public User()
        {
        }

        public User(Proxy.Models.IUser user)
        {
            if (user != null)
            {
                ImageUrl = user.ImageUrl;
                Id = user.Id;
                Name = user.Name;
            }
        }

        #endregion Public Constructors

        #region Public Properties

        public string Id { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }

        #endregion Public Properties
    }
}