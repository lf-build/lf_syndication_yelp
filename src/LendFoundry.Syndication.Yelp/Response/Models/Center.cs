﻿namespace LendFoundry.Syndication.Yelp.Response.Models
{
    public class Center : ICenter
    {
        #region Public Constructors

        public Center()
        {
        }

        public Center(Proxy.Models.ICenter center)
        {
            if (center != null)
            {
                Latitude = center.Latitude;
                Longitude = center.Longitude;
            }
        }

        #endregion Public Constructors

        #region Public Properties

        public double Latitude { get; set; }
        public double Longitude { get; set; }

        #endregion Public Properties
    }
}