﻿namespace LendFoundry.Syndication.Yelp.Response.Models
{
    public interface IUser
    {
        #region Public Properties

        string Id { get; set; }
        string ImageUrl { get; set; }
        string Name { get; set; }

        #endregion Public Properties
    }
}