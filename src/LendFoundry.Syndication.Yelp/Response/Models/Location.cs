﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Yelp.Response.Models
{
    public class Location : ILocation
    {
        #region Public Constructors

        public Location()
        {
        }

        public Location(Proxy.Models.ILocation location)
        {
            if (location != null)
            {
                Address1 = location.Address1;
                Address2 = location.Address2;
                Address3 = location.Address3;
                City = location.City;
                ZipCode = location.ZipCode;
                Country = location.Country;
                State = location.State;
                DisplayAddress = location.DisplayAddress;
            }
        }

        #endregion Public Constructors

        #region Public Properties
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public IList<string> DisplayAddress { get; set; }
        
        #endregion Public Properties
    }
}