﻿namespace LendFoundry.Syndication.Yelp.Response.Models
{
    public interface IRegion
    {
        #region Public Properties

        ICenter Center { get; set; }
        ISpan Span { get; set; }

        #endregion Public Properties
    }
}