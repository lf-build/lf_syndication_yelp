﻿namespace LendFoundry.Syndication.Yelp.Response.Models
{
    public class Span : ISpan
    {
        #region Public Constructors

        public Span()
        {
        }

        public Span(Proxy.Models.ISpan span)
        {
            if (span != null)
            {
                LatitudeDelta = span.LatitudeDelta;
                LongitudeDelta = span.LongitudeDelta;
            }
        }

        #endregion Public Constructors

        #region Public Properties

        public double LatitudeDelta { get; set; }
        public double LongitudeDelta { get; set; }

        #endregion Public Properties
    }
}