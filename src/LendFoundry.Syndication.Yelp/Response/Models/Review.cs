﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Yelp.Response.Models
{
    public class Review : IReview
    {
        #region Public Constructors

        public Review()
        {
        }

        public Review(Proxy.Models.IReview review)
        {
            if (review != null)
            {
                Rating = review.Rating;
                Excerpt = review.Excerpt;
                TimeCreated = review.TimeCreated;
                RatingImageUrl = review.RatingImageUrl;
                RatingImageSmallUrl = review.RatingImageSmallUrl;
                User = new User(review.User);
                RatingImageLargeUrl = review.RatingImageLargeUrl;
                Id = review.Id;
            }
        }

        #endregion Public Constructors

        #region Public Properties

        public string Excerpt { get; set; }
        public string Id { get; set; }
        public int Rating { get; set; }
        public string RatingImageLargeUrl { get; set; }
        public string RatingImageSmallUrl { get; set; }
        public string RatingImageUrl { get; set; }
        public int TimeCreated { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IUser, User>))]
        public IUser User { get; set; }

        #endregion Public Properties
    }
}