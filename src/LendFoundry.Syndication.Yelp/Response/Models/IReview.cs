﻿namespace LendFoundry.Syndication.Yelp.Response.Models
{
    public interface IReview
    {
        #region Public Properties

        string Excerpt { get; set; }
        string Id { get; set; }
        int Rating { get; set; }
        string RatingImageLargeUrl { get; set; }
        string RatingImageSmallUrl { get; set; }
        string RatingImageUrl { get; set; }
        int TimeCreated { get; set; }
        IUser User { get; set; }

        #endregion Public Properties
    }
}