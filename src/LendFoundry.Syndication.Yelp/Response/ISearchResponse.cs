﻿using LendFoundry.Syndication.Yelp.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Yelp.Response
{
    public interface ISearchResponse
    {
        #region Public Properties

        IList<IBusinessDetailsResponse> Businesses { get; set; }
        int Total { get; set; }

        #endregion Public Properties
    }
}