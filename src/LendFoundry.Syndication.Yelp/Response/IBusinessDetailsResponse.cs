﻿using LendFoundry.Syndication.Yelp.Proxy.Response;
using LendFoundry.Syndication.Yelp.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Yelp.Response
{
    public interface IBusinessDetailsResponse
    {
        #region Public Properties
        string Id { get; set; }

        string Name { get; set; }

        string ImageUrl { get; set; }

        bool IsClosed { get; set; }

        string Url { get; set; }

        int ReviewCount { get; set; }

        IList<ICategory> Categories { get; set; }

        double Rating { get; set; }

        Proxy.Response.ICoordinate Coordinates { get; set; }

        IList<string> Transaction { get; set; }

        ILocation Location { get; set; }

        string Phone { get; set; }

        string DisplayPhone { get; set; }
        #endregion Public Properties
    }
}