﻿using LendFoundry.Syndication.Yelp.Proxy.Response;
using LendFoundry.Syndication.Yelp.Response.Models;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Yelp.Response
{
    public class BusinessDetailResponse : IBusinessDetailsResponse
    {
        #region Public Constructors

        public BusinessDetailResponse()
        {
        }

        public BusinessDetailResponse(Proxy.Response.IBusiness business)
        {
            if (business != null)
            {
                Id = business.Id;
                Name = business.Name;
                ImageUrl = business.ImageUrl;
                IsClosed = business.IsClosed;
                Url = business.Url;
                ReviewCount = business.ReviewCount;
                Categories = business.Categories;
                Rating = business.Rating;
                Coordinates = business.Coordinates;
                Transaction = business.Transaction;
                Location = new Location(business.Location);
                Phone = business.Phone;
                DisplayPhone = business.DisplayPhone;
            }
        }

        #endregion Public Constructors

        #region Public Properties
        public string Id { get; set; }

        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public bool IsClosed { get; set; }

        public string Url { get; set; }

        public int ReviewCount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICategory, Category>))]
        public IList<ICategory> Categories { get; set; }

        public double Rating { get; set; }

        [JsonConverter(typeof(InterfaceConverter<Proxy.Response.ICoordinate, Proxy.Response.Coordinate>))]
        public Proxy.Response.ICoordinate Coordinates { get; set; }

        public IList<string> Transaction { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ILocation, Location>))]
        public ILocation Location { get; set; }

        public string Phone { get; set; }

        public string DisplayPhone { get; set; }

        #endregion Public Properties
    }
}