﻿using LendFoundry.Syndication.Yelp.Response;
using LendFoundry.Syndication.Yelp.Request;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Yelp
{
    /// <summary>
    /// Interface IYelpService
    /// </summary>
    public interface IYelpService
    {
        #region Public Methods

        /// <summary>
        /// Gets the business details.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="businessId">The business identifier.</param>
        /// <param name="countryCode">The country code.</param>
        /// <param name="language">The language.</param>
        /// <param name="langFilter">if set to <c>true</c> [language filter].</param>
        /// <param name="actionlinks">if its true then set action links.</param>
        /// <returns>BusinessDetailsResponse.</returns>
        Task<IBusinessDetailsResponse> GetBusinessDetails(string entityType, string entityId, string businessId, string countryCode, string language, bool langFilter, bool actionlinks);

        /// <summary>
        /// Gets the phone details.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="phoneNumber">The phone number.</param>
        /// <returns>SearchResponse.</returns>
        Task<ISearchResponse> GetPhoneDetails(string entityType, string entityId, string phoneNumber);

        /// <summary>
        /// Search the details.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="term">The term.</param>
        /// <param name="location">The location.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="sort">The sort.</param>
        /// <param name="categoryFilter">The category filter.</param>
        /// <param name="radiusFilter">The radius filter.</param>
        /// <param name="dealsFilter">if set to <c>true</c> [deals filter].</param>
        /// <returns>SearchResponse.</returns>
        Task<ISearchResponse> SearchDetails(string entityType, string entityId, string term, string location, int limit, int offset, int sort, string categoryFilter, int radiusFilter, bool dealsFilter);
        
        /// <summary>
        /// Business Match Details
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="Request"></param>
        /// <returns></returns>
        Task<ISearchResponse> GetBusinessMatchDetails(string entityType, string entityId, IBusinessMatchSearchRequest Request);
        #endregion Public Methods
    }
}