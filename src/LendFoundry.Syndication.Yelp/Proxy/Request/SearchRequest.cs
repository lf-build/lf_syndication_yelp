﻿using System;

namespace LendFoundry.Syndication.Yelp.Proxy.Request
{
    public class SearchRequest : ISearchRequest
    {
        #region Public Constructors

        public SearchRequest()
        {
        }

        public SearchRequest(Request.ISearchRequest request)
        {
            if (request != null)
                throw new ArgumentNullException(nameof(request));
            Term = request.Term;
            Location = request.Location;
            Latitude = request.Latitude;
            Longitude = request.Longitude;
            Radius = request.Radius;
            Categories = request.Categories;
            Locale = request.Locale;
            Limit = request.Limit;
            OffSet = request.OffSet;
            SortBy = request.SortBy;
            Price = request.Price;
            OpenNow = request.OpenNow;
            OpenAt = request.OpenAt;
            Attributes = request.Attributes;
        }

        #endregion Public Constructors

        #region Public Properties

        public string Attributes { get; set; }
        public string Categories { get; set; }
        public decimal Latitude { get; set; }
        public int Limit { get; set; }
        public string Locale { get; set; }
        public string Location { get; set; }
        public decimal Longitude { get; set; }
        public int OffSet { get; set; }
        public int OpenAt { get; set; }
        public bool OpenNow { get; set; }
        public string Price { get; set; }
        public int Radius { get; set; }
        public string SortBy { get; set; }
        public string Term { get; set; }

        #endregion Public Properties
    }
}