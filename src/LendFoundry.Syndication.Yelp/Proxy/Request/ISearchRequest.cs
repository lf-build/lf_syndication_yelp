﻿namespace LendFoundry.Syndication.Yelp.Proxy.Request
{
    public interface ISearchRequest
    {
        #region Public Properties

        string Attributes { get; set; }
        string Categories { get; set; }
        decimal Latitude { get; set; }
        int Limit { get; set; }
        string Locale { get; set; }
        string Location { get; set; }
        decimal Longitude { get; set; }
        int OffSet { get; set; }
        int OpenAt { get; set; }
        bool OpenNow { get; set; }
        string Price { get; set; }
        int Radius { get; set; }
        string SortBy { get; set; }
        string Term { get; set; }

        #endregion Public Properties
    }
}