namespace LendFoundry.Syndication.Yelp.Proxy.Request
{
    public class YelpBusinessMatchSearchRequest : IYelpBusinessMatchSearchRequest
    {
        
        public YelpBusinessMatchSearchRequest()
        {

        }

        public YelpBusinessMatchSearchRequest(Request.IYelpBusinessMatchSearchRequest request)
        {
            if (request != null)
                {
                    Name = request.Name;
                    Address1 = request.Address1;
                    Address2 = request.Address2;
                    Address3 = request.Address3;
                    City = request.City;
                    State = request.State;
                    Country = request.Country;
                    Latitude = request.Latitude;
                    Longitude = request.Longitude;
                    Phone = request.Phone;
                    Zip_Code = request.Zip_Code;
                    Yelp_Business_id = request.Yelp_Business_id;
                    Limit = request.Limit;
                    Match_Threshhold = request.Match_Threshhold;
                } 
        }

        public string Name {get; set;}

        public string Address1 {get;set;}

        public string Address2 {get; set;}

        public string Address3 {get;set;}

        public string City {get;set;}

        public string State {get;set;}

        public string Country {get;set;}

        public double Latitude{get;set;}

        public double Longitude {get; set;}

        public string Phone {get; set;}

        public string Zip_Code {get;set;}

        public string Yelp_Business_id {get; set;}

        public int Limit {get; set;}

        public string Match_Threshhold {get; set;}
    }
}