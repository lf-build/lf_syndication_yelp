﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Yelp.Proxy.Models
{
    public class User : IUser
    {
        #region Public Properties

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "image_url")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        #endregion Public Properties
    }
}