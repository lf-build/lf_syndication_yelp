﻿namespace LendFoundry.Syndication.Yelp.Proxy.Models
{
    public interface ICenter
    {
        #region Public Properties

        double Latitude { get; set; }
        double Longitude { get; set; }

        #endregion Public Properties
    }
}