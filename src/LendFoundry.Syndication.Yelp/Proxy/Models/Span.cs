﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Yelp.Proxy.Models
{
    public class Span : ISpan
    {
        #region Public Properties

        [JsonProperty(PropertyName = "latitude_delta")]
        public double LatitudeDelta { get; set; }

        [JsonProperty(PropertyName = "longitude_delta")]
        public double LongitudeDelta { get; set; }

        #endregion Public Properties
    }
}