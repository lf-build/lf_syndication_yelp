﻿namespace LendFoundry.Syndication.Yelp.Proxy.Models
{
    public interface ISpan
    {
        #region Public Properties

        double LatitudeDelta { get; set; }
        double LongitudeDelta { get; set; }

        #endregion Public Properties
    }
}