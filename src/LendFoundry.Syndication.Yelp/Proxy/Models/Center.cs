﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Yelp.Proxy.Models
{
    public class Center : ICenter
    {
        #region Public Properties

        [JsonProperty(PropertyName = "latitude")]
        public double Latitude { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        public double Longitude { get; set; }

        #endregion Public Properties
    }
}