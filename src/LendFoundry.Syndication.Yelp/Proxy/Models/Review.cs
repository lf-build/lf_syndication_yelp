﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Yelp.Proxy.Models
{
    public class Review : IReview
    {
        #region Public Properties

        [JsonProperty(PropertyName = "excerpt")]
        public string Excerpt { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "rating")]
        public int Rating { get; set; }

        [JsonProperty(PropertyName = "rating_image_large_url")]
        public string RatingImageLargeUrl { get; set; }

        [JsonProperty(PropertyName = "rating_image_small_url")]
        public string RatingImageSmallUrl { get; set; }

        [JsonProperty(PropertyName = "rating_image_url")]
        public string RatingImageUrl { get; set; }

        [JsonProperty(PropertyName = "time_created")]
        public int TimeCreated { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<User>))]
        [JsonProperty(PropertyName = "user")]
        public IUser User { get; set; }

        #endregion Public Properties
    }
}