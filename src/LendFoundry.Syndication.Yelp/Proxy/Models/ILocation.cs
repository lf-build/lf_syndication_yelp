﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Yelp.Proxy.Models
{
    public interface ILocation
    {
        #region Public Properties

        string Address1 { get; set; }
        string Address2 { get; set; }
        string Address3 { get; set; }
        string City { get; set; }
        string ZipCode { get; set; }
        string Country { get; set; }
        string State { get; set; }
        IList<string> DisplayAddress { get; set; }

        #endregion Public Properties
    }
}