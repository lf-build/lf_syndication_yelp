﻿using LendFoundry.Syndication.Yelp.Proxy.Response;
using LendFoundry.Syndication.Yelp.Proxy.Request;

namespace LendFoundry.Syndication.Yelp.Proxy
{
    public interface IYelpProxy
    {
        #region Public Methods

        IBusiness GetBusinessDetails(string businessId, string countryCode, string language, bool langFilter, bool actionlinks);

        ISearchResponse GetPhoneDetails(string phoneNumber);

        ISearchResponse SearchDetails(string term, string location, int limit, int offset, int sort, string categoryFilter, int radiusFilter, bool dealsFilter);

        ISearchResponse GetBusinessMatchDetails(IYelpBusinessMatchSearchRequest Request);

        #endregion Public Methods
    }
}