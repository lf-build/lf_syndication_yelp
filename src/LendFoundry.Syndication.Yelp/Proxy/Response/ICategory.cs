﻿namespace LendFoundry.Syndication.Yelp.Proxy.Response
{
    public interface ICategory
    {
       string Alias { get; set; }
       string Title { get; set; }
    }
}
