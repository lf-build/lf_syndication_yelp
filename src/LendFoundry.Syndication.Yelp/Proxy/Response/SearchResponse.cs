﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Yelp.Proxy.Response
{
    public class SearchResponse : ISearchResponse
    {
        #region Public Properties

        [JsonConverter(typeof(ConcreteListJsonConverter<List<Business>, IBusiness>))]
        [JsonProperty(PropertyName = "businesses")]
        public IList<IBusiness> Businesses { get; set; }

        [JsonProperty(PropertyName = "total")]
        public int Total { get; set; }

        #endregion Public Properties
    }
}