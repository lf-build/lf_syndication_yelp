﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Yelp.Proxy.Response
{
    public class Category:ICategory
    {
        [JsonProperty(PropertyName = "alias")]
        public string Alias { get; set; }
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }
    }
}
