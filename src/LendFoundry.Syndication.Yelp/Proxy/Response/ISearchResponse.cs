﻿using LendFoundry.Syndication.Yelp.Proxy.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Yelp.Proxy.Response
{
    public interface ISearchResponse
    {
        #region Public Properties

        IList<IBusiness> Businesses { get; set; }
        int Total { get; set; }

        #endregion Public Properties
    }
}