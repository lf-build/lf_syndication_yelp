﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Yelp.Proxy.Response
{
    public interface ICoordinate
    {
        string Latitude { set; get; }
        string Longitude { set; get; }

    }
}
