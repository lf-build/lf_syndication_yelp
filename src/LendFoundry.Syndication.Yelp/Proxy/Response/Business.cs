﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Yelp.Proxy.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Yelp.Proxy.Response
{
    public class Business : IBusiness
    {
        #region Public Properties
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "image_url")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "is_closed")]
        public bool IsClosed { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "review_count")]
        public int ReviewCount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICategory, Category>))]
        [JsonProperty(PropertyName = "categories")]
        public IList<ICategory> Categories { get; set; }

        [JsonProperty(PropertyName = "rating")]
        public double Rating { get; set; }

        [JsonProperty(PropertyName = "coordinates")]
        [JsonConverter(typeof(InterfaceConverter<ICoordinate, Coordinate>))]
        public ICoordinate Coordinates { get; set; }
        

        [JsonProperty(PropertyName = "transactions")]
        public IList<string> Transaction { get; set; }

        [JsonProperty(PropertyName = "location")]
        [JsonConverter(typeof(InterfaceConverter<ILocation, Location>))]
        public ILocation Location { get; set; }

        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "display_phone")]
        public string DisplayPhone { get; set; }
        #endregion Public Properties
    }
}