﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Yelp.Proxy.Response
{
    public class Coordinate:ICoordinate
    {
        [JsonProperty(PropertyName = "latitude")]
        public string Latitude { set; get; }

        [JsonProperty(PropertyName = "longitude")]
        public string Longitude { set; get; }
    }
}
