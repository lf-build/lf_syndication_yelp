﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using LendFoundry.Syndication.Yelp.Proxy.Request;
using LendFoundry.Syndication.Yelp.Proxy.Response;
using Newtonsoft.Json;
using RestSharp;
using SimpleOAuth;

namespace LendFoundry.Syndication.Yelp.Proxy {
    public class YelpProxy : IYelpProxy {
        #region Public Constructors

        public YelpProxy (IYelpConfiguration configuration) {
            if (configuration == null)
                throw new ArgumentNullException (nameof (configuration));

            if (string.IsNullOrWhiteSpace (configuration.ConsumerKey))
                throw new ArgumentNullException (nameof (configuration.ConsumerSecret));

            if (string.IsNullOrWhiteSpace (configuration.Token))
                throw new ArgumentNullException (nameof (configuration.TokenSecret));

            if (string.IsNullOrWhiteSpace (configuration.ApiBaseUrl))
                throw new ArgumentNullException (nameof (configuration.ApiBaseUrl));

            if (configuration.PhonePrefix == null)
                throw new ArgumentNullException (nameof (configuration.PhonePrefix));
            Configuration = configuration;
        }

        #endregion Public Constructors

        #region Private Properties

        private IYelpConfiguration Configuration { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// Gets the business details.
        /// </summary>
        /// <param name="businessId">The business identifier.</param>
        /// <param name="countryCode">The country code.</param>
        /// <param name="language">The language.</param>
        /// <param name="langFilter">if set to <c>true</c> [language filter].</param>
        /// <param name="actionlinks">if set to <c>true</c> [actionlinks].</param>
        /// <returns>IBusiness.</returns>
        /// <exception cref="System.ArgumentException">Value cannot be null or whitespace. - businessId</exception>
        public IBusiness GetBusinessDetails (string businessId, string countryCode, string language, bool langFilter, bool actionlinks) {
            if (string.IsNullOrWhiteSpace (businessId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (businessId));
            string url = $"{Configuration.ApiBaseUrl.TrimEnd('/')}{Configuration.BusinessApiUrl.TrimEnd('/')}/{businessId}";
            var queryParams = new Dictionary<string, string> ();
            if (!string.IsNullOrWhiteSpace (countryCode))
                queryParams.Add ("cc", countryCode);
            if (!string.IsNullOrWhiteSpace (language))
                queryParams.Add ("lang", language);
            if (langFilter)
                queryParams.Add ("lang_filter", langFilter.ToString ());
            if (actionlinks)
                queryParams.Add ("actionlinks", actionlinks.ToString ());
            return ExecuteRequest<Business> (url);
        }

        /// <summary>
        /// Gets the phone details.
        /// </summary>
        /// <param name="phoneNumber">The phone number.</param>
        /// <returns>ISearchResponse.</returns>
        /// <exception cref="System.ArgumentException">Value cannot be null or whitespace. - phoneNumber</exception>
        public ISearchResponse GetPhoneDetails (string phoneNumber) {
            if (string.IsNullOrWhiteSpace (phoneNumber))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (phoneNumber));
            var baseUri = new Uri (Configuration.ApiBaseUrl);
            var client = new RestClient (baseUri);
            IRestRequest request = new RestRequest (Configuration.PhoneApiUrl.TrimEnd ('/'), Method.GET);
            request.AddParameter ("phone", Configuration.PhonePrefix + phoneNumber);
            request.AddHeader ("Accept", "application/json");
            request.AddHeader ("Authorization", $"Bearer {Configuration.Token}");
            return ExecuteRequest<SearchResponse> (request, client);
        }

        public ISearchResponse GetBusinessMatchDetails (IYelpBusinessMatchSearchRequest requestModel) {
            if (requestModel == null)
                throw new ArgumentNullException ("Request can not be null");

            var baseUri = new Uri (Configuration.ApiBaseUrl);
            var client = new RestClient (baseUri);
            IRestRequest request = new RestRequest (Configuration.BusinessMatchApiUrl.TrimEnd ('/'), Method.GET);
            request.AddHeader ("Accept", "application/json");
            request.AddHeader ("Authorization", $"Bearer {Configuration.Token}");
            request.AddParameter ("name", requestModel.Name.ToString ());
            request.AddParameter ("address1", requestModel.Address1.ToString ());
            request.AddParameter ("city", requestModel.City.ToString ());
            request.AddParameter ("state", requestModel.State.ToString ());
            request.AddParameter ("country", requestModel.Country.ToString ());
            if (!string.IsNullOrWhiteSpace (requestModel.Address2))
                request.AddParameter ("address2", requestModel.Address2.ToString ());
            if (!string.IsNullOrWhiteSpace (requestModel.Address3))
                request.AddParameter ("address3", requestModel.Address3.ToString ());
            if (!string.IsNullOrWhiteSpace (requestModel.Phone))
                request.AddParameter ("phone", Configuration.PhonePrefix + requestModel.Phone);
            if (!string.IsNullOrWhiteSpace (requestModel.Zip_Code))
                request.AddParameter ("zip_code", requestModel.Zip_Code.ToString ());
            if (!string.IsNullOrWhiteSpace (requestModel.Yelp_Business_id))
                request.AddParameter ("yelp_business_id", requestModel.Yelp_Business_id.ToString ());
            if (requestModel.Limit > 0)
                request.AddParameter ("limit", requestModel.Limit.ToString ());
            if (requestModel.Latitude > 0)
                request.AddParameter ("latitude", requestModel.Latitude.ToString ());
            if (requestModel.Longitude > 0)
                request.AddParameter ("longitude", requestModel.Longitude.ToString ());
            if (!string.IsNullOrWhiteSpace (requestModel.Match_Threshhold))
                request.AddParameter ("match_threshold", requestModel.Match_Threshhold.ToString ());
            return ExecuteRequest<SearchResponse> (request, client);
        }

        public ISearchResponse SearchDetails (string term, string location, int limit, int offset,
            int sort, string categoryFilter, int radiusFilter, bool dealsFilter) {
            var baseUri = new Uri (Configuration.ApiBaseUrl);
            var client = new RestClient (baseUri);
            IRestRequest request = new RestRequest (Configuration.SearchApiUrl.TrimEnd ('/'), Method.GET);
            request.AddHeader ("Accept", "application/json");
            request.AddHeader ("Authorization", $"Bearer {Configuration.Token}");
            request.AddParameter ("term", term.ToString ());
            request.AddParameter ("location", location.ToString ());
            if (limit != 0)
                request.AddParameter ("limit", limit.ToString ());
            if (offset != 0)
                request.AddParameter ("offset", offset.ToString ());
            if (sort != 0)
                request.AddParameter ("sort", sort.ToString ());
            if (radiusFilter != 0)
                request.AddParameter ("radius_filter", radiusFilter.ToString ());
            if (dealsFilter)
                request.AddParameter ("deals_filter", dealsFilter.ToString ());
            if (!string.IsNullOrWhiteSpace (categoryFilter))
                request.AddParameter ("category_filter", categoryFilter);
            return ExecuteRequest<SearchResponse> (request, client);
        }

        #endregion Public Methods

        #region Private Methods
        private T ExecuteRequest<T> (IRestRequest req, IRestClient client) {
            var response = client.Execute (req);
            if (response.ErrorMessage != null)
                throw new Exception ($"Error message is{response.ErrorMessage}");
            if (response == null)
                throw new ArgumentNullException (nameof (response));
            if (response.Content == null)
                throw new ArgumentNullException (nameof (response.Content));
            try {
                return JsonConvert.DeserializeObject<T> (response.Content);
            } catch (Exception exception) {
                throw new Exception ("Unable to deserialize Response", exception);
            }
        }

        /// <summary>
        /// Executes the request.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url">The URL.</param>
        /// <param name="queryParams">The query parameters.</param>
        /// <param name="method">The method.</param>
        /// <returns>T.</returns>
        /// <exception cref="System.ArgumentNullException">
        /// response
        /// or
        /// responseContent
        /// </exception>
        /// <exception cref="System.Exception">Unable to deserialize:" + responseContent</exception>
        private T ExecuteRequest<T> (string url, Dictionary<string, string> queryParams = null, string method = "GET") {
            string queryString = string.Empty;
            if (queryParams != null && queryParams.Count > 0) {
                queryString = $"{string.Join("&", queryParams.Select(param => string.Format($"{ param.Key}={param.Value}")))}";
            }

            var uriBuilder = new UriBuilder (url);
            uriBuilder.Query = queryString;

            var request = WebRequest.Create (uriBuilder.ToString ());
            request.Method = method;

            request.SignRequest (
                new Tokens {
                    ConsumerKey = Configuration.ConsumerKey,
                        ConsumerSecret = Configuration.ConsumerSecret,
                        AccessToken = Configuration.Token,
                        AccessTokenSecret = Configuration.TokenSecret
                }
            ).WithEncryption (EncryptionMethod.HMACSHA1).InHeader ();

            HttpWebResponse response = (HttpWebResponse) request.GetResponse ();
            var stream = new StreamReader (response.GetResponseStream (), Encoding.UTF8);
            var responseContent = stream.ReadToEnd ();
            if (response == null)
                throw new ArgumentNullException (nameof (response));
            if (responseContent == null)
                throw new ArgumentNullException (nameof (responseContent));
            try {
                return JsonConvert.DeserializeObject<T> (responseContent);
            } catch (Exception exception) {
                throw new Exception ("Unable to deserialize Response", exception);
            }
        }

        #endregion Private Methods
    }
}