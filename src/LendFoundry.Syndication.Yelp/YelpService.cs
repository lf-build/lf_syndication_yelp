﻿using LendFoundry.Syndication.Yelp.Events;
using LendFoundry.Syndication.Yelp.Proxy;
using LendFoundry.Syndication.Yelp.Request;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Yelp
{
    public class YelpService : IYelpService
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="YelpService"/> class.
        /// </summary>
        /// <param name="proxy">The proxy for Yelp.</param>
        /// <param name="eventHub">The EventHubClient object.</param>
        /// <param name="lookup">The lookupService object.</param>

        public YelpService(IYelpProxy proxy, IEventHubClient eventHub, ILookupService lookup,ILogger logger,ITenantTime tenantTime)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
            Proxy = proxy;
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            EventHub = eventHub;
            if (lookup == null)
                throw new ArgumentNullException(nameof(lookup));
            Lookup = lookup;
            Logger = logger;
            TenantTime = tenantTime;
        }

        #endregion Public Constructors

        #region Private Properties

        private IEventHubClient EventHub { get; set; }
        private ILookupService Lookup { get; }
        private IYelpProxy Proxy { get; set; }
        private ILogger Logger { get; set; }
        private ITenantTime TenantTime { get; set; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// Ensures the EntityType
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>System.String.</returns>

        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

        /// <summary>
        /// Gets the business details.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="businessId">The business identifier.</param>
        /// <param name="countryCode">The country code.</param>
        /// <param name="language">The language.</param>
        /// <param name="langFilter">if set to <c>true</c> [language filter].</param>
        /// <param name="actionlinks">if its true then set  action link.</param>
        /// <returns>BusinessDetailsResponse.</returns>

        public async Task<Response.IBusinessDetailsResponse> GetBusinessDetails(string entityType, string entityId, string businessId, string countryCode, string language, bool langFilter, bool actionlinks)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(businessId))
                throw new ArgumentException("BusinessId cannot be null or whitespace.", nameof(businessId));
            try
            {
                Logger.Info("Started Execution for GetBusinessDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Yelp: GetBusinessDetails");

                var response = Proxy.GetBusinessDetails(businessId, countryCode, language, langFilter, actionlinks);
                var referencenumber = Guid.NewGuid().ToString("N");
                var result = new Response.BusinessDetailResponse(response);
                await EventHub.Publish(new YelpServiceGetBusinessDetailsRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = new { BusinessId = businessId, CountryCode = countryCode, Language = language, LangFilter = langFilter, Actionlinks = actionlinks },
                    ReferenceNumber = referencenumber
                });
                Logger.Info("Completed Execution for GetBusinessDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Yelp: GetBusinessDetails");

                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetBusinessDetails Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Yelp: GetBusinessDetails");

                await EventHub.Publish(new YelpServiceGetBusinessDetailsRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = new { BusinessId = businessId, CountryCode = countryCode, Language = language, LangFilter = langFilter, Actionlinks = actionlinks },
                    ReferenceNumber = null
                });
                throw ;
            }
        }

        /// <summary>
        /// Gets the phone details.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="phoneNumber">The phone number.</param>
        /// <returns>SearchResponse</returns>

        public async Task<Response.ISearchResponse> GetPhoneDetails(string entityType, string entityId, string phoneNumber)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(phoneNumber))
                throw new ArgumentNullException(nameof(phoneNumber));
            string phoneNumberPattern = "^(0|[1-9][0-9]+)$";
            if (!Regex.IsMatch(phoneNumber, phoneNumberPattern))
                throw new ArgumentException("Invalid Phone Number format");
            try
            {
                Logger.Info("Started Execution for GetPhoneDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Yelp: GetPhoneDetails");

                var response = Proxy.GetPhoneDetails(phoneNumber);
                var result = new Response.SearchResponse(response);
                await EventHub.Publish(new YelpServiceGetPhoneDetailsRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = phoneNumber,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
            });
                Logger.Info("Completed Execution for GetPhoneDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Yelp: GetPhoneDetails");

                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetPhoneDetails Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Yelp: GetPhoneDetails");

                await EventHub.Publish(new YelpServiceGetPhoneDetailsRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = phoneNumber,
                    ReferenceNumber = null
                });
                throw ;
            }
        }

        /// <summary>
        /// Search the details of business.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="term">Business Name.</param>
        /// <param name="location">Specify combination of address, city, state or zip.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="sort">The sort.</param>
        /// <param name="categoryFilter">The category filter.</param>
        /// <param name="radiusFilter">The radius filter.</param>
        /// <param name="dealsFilter">if set to <c>true</c> [deals filter].</param>
        /// <returns>ISearchResponse.</returns>

        public async Task<Response.ISearchResponse> SearchDetails(string entityType, string entityId, string term, string location, int limit, int offset, int sort, string categoryFilter, int radiusFilter, bool dealsFilter)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrEmpty(term))
                throw new ArgumentNullException(nameof(term));
            if (string.IsNullOrEmpty(location))
                throw new ArgumentNullException(nameof(location));
            try
            {
                Logger.Info("Started Execution for SearchDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Yelp: SearchDetails");

                var response = Proxy.SearchDetails(term, location, limit, offset, sort, categoryFilter, radiusFilter, dealsFilter);
                var result = new Response.SearchResponse(response);
                await EventHub.Publish(new YelpServiceSearchDetailsRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = new { term = term, Location = location, Limit = limit, Offset = offset, Sort = sort, CategoryFilter = categoryFilter, RadiusFilter = radiusFilter, DealsFilter = dealsFilter },
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                Logger.Info("Completed Execution for SearchDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Yelp: SearchDetails");

                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing SearchDetails Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Yelp: SearchDetails");

                await EventHub.Publish(new YelpServiceSearchDetailsRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = new { term = term, Location = location, Limit = limit, Offset = offset, Sort = sort, CategoryFilter = categoryFilter, RadiusFilter = radiusFilter, DealsFilter = dealsFilter },
                    ReferenceNumber = null
                });
                throw ;
            }
        }

        public async Task<Response.ISearchResponse> GetBusinessMatchDetails(string entityType, string entityId,IBusinessMatchSearchRequest requestModel)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException("EntityId can not be null");
            if (string.IsNullOrWhiteSpace(requestModel.Name))
                throw new ArgumentNullException("Name can not be null");
            if (string.IsNullOrWhiteSpace(requestModel.Address1))
                throw new ArgumentNullException("Address1 can not be null");
            if (string.IsNullOrWhiteSpace(requestModel.City))
                throw new ArgumentNullException("City can not be null");
            if (string.IsNullOrWhiteSpace(requestModel.State))
                throw new ArgumentNullException("State can not be null");
            if (string.IsNullOrWhiteSpace(requestModel.Country))
                throw new ArgumentNullException("Country can not be null");

            try{
                Logger.Info("Started Execution for BusinessMatch Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Yelp: BusinessMatch");

                LendFoundry.Syndication.Yelp.Proxy.Request.YelpBusinessMatchSearchRequest req = new LendFoundry.Syndication.Yelp.Proxy.Request.YelpBusinessMatchSearchRequest();
                req.Name = requestModel.Name;
                req.Address1 = requestModel.Address1;
                req.Address2 = requestModel.Address2;
                req.Address3 = requestModel.Address3;
                req.City = requestModel.City;
                req.State = requestModel.State;
                req.Country = requestModel.Country;
                req.Latitude = requestModel.Latitude;
                req.Longitude = requestModel.Longitude;
                req.Phone = requestModel.Phone;
                req.Zip_Code = requestModel.Zip_Code;
                req.Yelp_Business_id = requestModel.Yelp_Business_id;
                req.Limit = requestModel.Limit;
                req.Match_Threshhold = requestModel.Match_Threshhold;
                var response = Proxy.GetBusinessMatchDetails(req);
                var result = new Response.SearchResponse(response);
                await EventHub.Publish(new YelpBusinessMatchDetailsRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = new { name = requestModel.Name, address1 = requestModel.Address1,city = requestModel.City,state = requestModel.State, country = requestModel.Country  },
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                Logger.Info("Completed Execution for BusinessMatch Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Yelp: SearchDetails");

                return result;

            }
            catch(Exception exception)
            {
                Logger.Error("Error While Processing BusinessMatch Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Yelp: SearchDetails");

                await EventHub.Publish(new YelpBusinessMatchDetailsRequestedFailed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = new { name = requestModel.Name, address1 = requestModel.Address1,city = requestModel.City,state = requestModel.State, country = requestModel.Country  },
                    ReferenceNumber = null
                });
                throw ;
                
            }
        }

        #endregion Public Methods
    }
}