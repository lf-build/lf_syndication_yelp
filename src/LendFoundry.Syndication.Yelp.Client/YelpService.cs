﻿using LendFoundry.Syndication.Yelp;
using LendFoundry.Syndication.Yelp.Response;
using LendFoundry.Syndication.Yelp.Request;
using LendFoundry.Foundation.Client;
using RestSharp;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Yelp.Client
{
    public class YelpService : IYelpService
    {
        public YelpService(IServiceClient client)
        {
            Client = client;
        }
        
        private IServiceClient Client { get; }

        /// <summary>
        /// Gets the business details.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="businessId">The business identifier.</param>
        /// <param name="countryCode">The country code.</param>
        /// <param name="language">The language.</param>
        /// <param name="langFilter">if its true then set language filter.</param>
        /// <param name="actionlinks">if its true then set action links.</param>
        /// <returns>BusinessDetailsResponse.</returns>

        public async Task<IBusinessDetailsResponse> GetBusinessDetails(string entityType, string entityId, string businessId, string countryCode, string language, bool langFilter, bool actionlinks)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(businessId))
                throw new ArgumentNullException(nameof(businessId));
            var request = new RestRequest("/{entitytype}/{entityid}/{businessId}/businesses", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("businessId", businessId);
            request.AddParameter("countryCode", countryCode);
            request.AddParameter("language", language);
            request.AddParameter("langFilter", langFilter);
            request.AddParameter("actionlinks", actionlinks);
            return await Client.ExecuteAsync<BusinessDetailResponse>(request);
        }

        /// <summary>
        /// Gets the phone details.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="phoneNumber">The phone number.</param>
        /// <returns>Response include phone details.</returns>

        public async Task<ISearchResponse> GetPhoneDetails(string entityType, string entityId, string phoneNumber)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(phoneNumber))
                throw new ArgumentNullException(nameof(phoneNumber));
            string phoneNumberPattern = "^(0|[1-9][0-9]+)$";
            if (!Regex.IsMatch(phoneNumber, phoneNumberPattern))
                throw new ArgumentException("Invalid Phone Number format");
            var request = new RestRequest("/{entitytype}/{entityid}/businesses/search/{phoneNumber}/phone", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("phoneNumber", phoneNumber);
            return await Client.ExecuteAsync<SearchResponse>(request);
        }

        /// <summary>
        /// Search the details.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="term">The term.</param>
        /// <param name="location">The location.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="sort">The sort.</param>
        /// <param name="categoryFilter">The category filter.</param>
        /// <param name="radiusFilter">The radius filter.</param>
        /// <param name="dealsFilter">if its true then set deals filter.</param>
        /// <returns>SearchResponse.</returns>

        public async Task<ISearchResponse> SearchDetails(string entityType, string entityId, string term, string location, int limit, int offset, int sort, string categoryFilter, int radiusFilter, bool dealsFilter)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(term))
                throw new ArgumentNullException(nameof(term));
            if (string.IsNullOrWhiteSpace(location))
                throw new ArgumentNullException(nameof(location));
            var request = new RestRequest("/{entitytype}/{entityid}/businesses/search/{term}/{location}", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("location", location);
            request.AddUrlSegment("term", term);
            request.AddParameter("limit", limit);
            request.AddParameter("offset", offset);
            request.AddParameter("sort", sort);
            request.AddParameter("categoryFilter", categoryFilter);
            request.AddParameter("radiusFilter", radiusFilter);
            request.AddParameter("dealsFilter", dealsFilter);
            return await Client.ExecuteAsync<SearchResponse>(request);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        public async Task<ISearchResponse> GetBusinessMatchDetails(string entityType, string entityId, IBusinessMatchSearchRequest requestModel)
        {
            if(requestModel == null)
                throw new ArgumentNullException("Request can notbe null");
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException("EntityType can not be null");
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException("EntityId can not be null");
            if (string.IsNullOrWhiteSpace(requestModel.Name))
                throw new ArgumentNullException("Name can not be null");
            if (string.IsNullOrWhiteSpace(requestModel.Address1))
                throw new ArgumentNullException("Address1 can not be null");
            if (string.IsNullOrWhiteSpace(requestModel.City))
                throw new ArgumentNullException("City can not be null");
            if (string.IsNullOrWhiteSpace(requestModel.State))
                throw new ArgumentNullException("State can not be null");
            if (string.IsNullOrWhiteSpace(requestModel.Country))
                throw new ArgumentNullException("Country can not be null");

            var request = new RestRequest("/{entitytype}/{entityid}/businesses/match", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            if (requestModel != null)
                request.AddJsonBody(requestModel);
            else
                request.AddJsonBody(new BusinessMatchSearchRequest());

            return await Client.ExecuteAsync<SearchResponse>(request);

        }
    }
}