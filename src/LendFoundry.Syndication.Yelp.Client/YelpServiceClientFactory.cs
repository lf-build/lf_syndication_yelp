﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Yelp.Client
{
    public class YelpServiceClientFactory : IYelpServiceClientFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public YelpServiceClientFactory(IServiceProvider provider, string endPoint, int port)
        {
            Provider = provider;
            EndPoint = endPoint;
            Port = port;
            Uri = new UriBuilder("http", EndPoint, Port).Uri;
        }
        public YelpServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        private IServiceProvider Provider { get; }
        private string EndPoint { get; }
        private int Port { get; }
        private Uri Uri { get; }
        public IYelpService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("yelp");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new YelpService(client);
        }
    }
}