﻿using LendFoundry.Syndication.Yelp;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Yelp.Client
{
    public interface IYelpServiceClientFactory
    {
        #region Public Methods

        IYelpService Create(ITokenReader reader);

        #endregion Public Methods
    }
}