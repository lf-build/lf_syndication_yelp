﻿using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;
namespace LendFoundry.Syndication.Yelp.Client
{
    public static class YelpServiceClientExtentions
    {
        #region Public Methods

        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddYelpService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IYelpServiceClientFactory>(p => new YelpServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IYelpServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        public static IServiceCollection AddYelpService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IYelpServiceClientFactory>(p => new YelpServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IYelpServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddYelpService(this IServiceCollection services)
        {
            services.AddSingleton<IYelpServiceClientFactory>(p => new YelpServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IYelpServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        #endregion Public Methods
    }
}